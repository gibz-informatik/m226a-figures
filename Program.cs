﻿using System.Collections.Generic;

namespace Inheritance_Basics
{
    class Program
    {
        static void Main(string[] args)
        {
            #region INHERITANCE

            if (true)
            {
                Figure figure = new Figure();
                figure.GetPosition();
                figure.PrintName();         // FIGURE
                figure.PrintScaleFactor();  // 1

                System.Console.WriteLine("\n  ***  \n");

                Circle circle = new Circle();
                circle.GetPosition();
                circle.PrintName();         // FIGURE
                circle.PrintNameOfCircle(); // CIRCLE / FIGURE
                circle.PrintScaleFactor();          // 1
                circle.PrintScaleFactorOfCircle();  // ?

                System.Console.WriteLine("\n  ***  \n");

                Rectangle rectangle = new Rectangle();
                rectangle.GetPosition();
                rectangle.Rotate90DegreeClockwise();
                rectangle.GetPosition();
            }

            #endregion INHERITANCE


            #region POLYMORPHISM I

            if (false)
            {
                // Liste definieren
                List<Figure> list = new List<Figure>();

                // Elemente zur Liste hinzufügen
                list.Add(new Figure());
                list.Add(new Circle());
                list.Add(new Rectangle());

                // Methode Draw auf Elemente in Liste aufrufen
                foreach (Figure figure in list)
                {
                    figure.Draw();
                }
            }

            #endregion POLYMORPHISM I


            #region POLYMORPHISM II

            if (false)
            {
                BaseClass bc = new BaseClass();
                DerivedClass dc = new DerivedClass();
                BaseClass bcdc = new DerivedClass();

                bc.MethodA();   // Base A
                bc.MethodB();   // Base B
                dc.MethodA();   // Base A
                dc.MethodB();   // Derived B
                bcdc.MethodA(); // Base A
                bcdc.MethodB(); // Base B
            }

            #endregion POLYMORPHISM II
        }
    }
}
