﻿using System;

namespace Inheritance_Basics
{
    public class Figure
    {
        protected double _xPosition = 100.0;
        protected double _yPosition = 100.0;

        protected double _scaleFactor = 1.0;

        public Figure(double xPosition, double yPosition)
        {
            _xPosition = xPosition;
            _yPosition = yPosition;
        }

        public Figure() { }

        public void GetPosition()
        {
            Console.WriteLine(
                $"Top left corner is at x={_xPosition}," +
                $"y={_yPosition}"
            );
        }

        public void PrintName()
        {
            Console.WriteLine("It's a FIGURE");
        }

        public void PrintScaleFactor()
        {
            Console.WriteLine(
                $"Scale factor of figure: {this._scaleFactor}"
            );
        }

        #region POLYMORPHISM

        public virtual void Draw()
        {
            Console.WriteLine("Drawing figure");
        }

        #endregion POLYMORPHISM

    }
}
