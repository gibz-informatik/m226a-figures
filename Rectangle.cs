﻿using System;

namespace Inheritance_Basics
{
    /// <summary>
    /// Die Klasse Rectangle ist eine Spezialisierung der Klasse Figure.
    /// Die Klasse Figure ist die Superklasse (oder: Basisklasse) der Klasse Rectangle.
    /// </summary>
    public class Rectangle : Figure
    {
        private double _width = 65.0;
        private double _height = 40.0;

        public double Width
        {
            get => _width;
            set { if (value >= 0.0) _width = value; }
        }

        public double Height
        {
            get => _height;
            set { if (value >= 0.0) _height = value; }
        }

        /// <summary>
        /// Dieser Konstruktor basiert auf einem Konstruktor der Superklasse mit zwei Parametern des Typs double.
        /// Der Konstruktor der Superklasse wird durch den Aufruf base(xPosition, yPosition) vor der Ausführung
        /// dieses Konstruktors der Klasse Rectangle ausgeführt.
        /// </summary>
        /// <param name="xPosition">X-Koordinate des Rechtecks</param>
        /// <param name="yPosition">Y-Koordinate des Rechtecks</param>
        /// <param name="width">Breite des Rechtecks</param>
        /// <param name="height">Höhe des Rechtecks</param>
        public Rectangle(double xPosition, double yPosition, double width, double height) : base(xPosition, yPosition)
        {
            Width = width;
            Height = height;
        }

        public Rectangle() { }

        /// <summary>
        /// Diese Methode rotiert das Reckteck um 90 Grad im Uhrzeigersinn.
        /// Der Fixpunkt für die Rotation ist die untere, rechte Ecke des (ursprünglichen) Recktecks.
        /// </summary>
        public void Rotate90DegreeClockwise()
        {
            // TODO: Implement this method

            #region SOLUTION

            // Neue Koordinaten definieren
            _xPosition += _width;
            _yPosition = _yPosition + _height - _width;

            // Dimensionen des Recktecks anpassen
            double initialWidth = _width;
            _width = _height;
            _height = initialWidth;

            #endregion SOLUTION
        }

        /// <summary>
        /// In dieser Methode wird die Verwendung des BASE Schlüsselwortes gezeigt.
        /// Wenn die Funktionalität aus der Basisklasse erweitert/ergänzt werden soll,
        /// können Sie innerhalb der Methode die gleichnamige (oder auch eine andere)
        /// Methode mit dem base Schlüsselwort aufrufen.
        /// Diese GetPosition Methode gibt die Position der oberen/linken Ecke
        /// und ZUSÄTZLICH von der unteren/rechten Ecke auf die Konsole aus.
        /// </summary>
        public new void GetPosition()
        {
            base.GetPosition();
            Console.WriteLine(
                $"Bottom right corner is at x={_xPosition + _width}," +
                $"y={_yPosition + _height}"
            );
        }

        #region POLYMORPHISM

        public override void Draw()
        {
            Console.WriteLine($"Drawing rectangle with width={_width} and height={_height}");
        }

        #endregion POLYMORPHISM
    }
}
