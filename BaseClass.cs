﻿namespace Inheritance_Basics
{
    class BaseClass
    {
        public virtual void MethodA()
        {
            System.Console.WriteLine("Base - Method A");
        }

        public void MethodB()
        {
            System.Console.WriteLine("Base - Method B");
        }
    }

    class DerivedClass : BaseClass
    {
        public override void MethodA()
        {
            System.Console.WriteLine("Derived - Method A");
        }

        public new void MethodB()
        {
            System.Console.WriteLine("Derived - Method B");
        }
    }
}
