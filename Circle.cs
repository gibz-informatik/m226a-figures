﻿using System;

namespace Inheritance_Basics
{
    /// <summary>
    /// Die Klasse Circle ist eine Spezialisierung der Klasse Figure.
    /// Die Klasse Figure ist die Superklasse (oder: Basisklasse) der Klasse Circle.
    /// </summary>
    public class Circle : Figure
    {
        private double _radius = 75.0;

        // Dieses Feld ist bewusst PUBLIC um zu zeigen, dass der Zugriffsmodifizierer
        // bei der Verdeckung von Instanzvariablen für das Verhalten NICHT relevant ist
        public new string _scaleFactor = "high";

        public double Radius
        {
            get => _radius;
            set
            {
                if (value >= 0.0)
                {
                    _radius = value;
                }
            }
        }

        /// <summary>
        /// Dieser Konstruktor basiert auf einem Konstruktor der Superklasse mit zwei Parametern des Typs double.
        /// Der Konstruktor der Superklasse wird durch den Aufruf base(xPosition, yPosition) vor der Ausführung
        /// dieses Konstruktors der Klasse Circle ausgeführt.
        /// </summary>
        /// <param name="xPosition">X-Koordinate des Kreis</param>
        /// <param name="yPosition">Y-Koordinate des Kreis</param>
        /// <param name="radius">Radius des Kreis</param>
        public Circle(double xPosition, double yPosition, double radius) : base(xPosition, yPosition)
        {
            Radius = radius;
        }

        /// <summary>
        /// Ein parameterloser Konstruktor wird hinzugefügt, um Objekte der Klasse Circle ohne Angabe
        /// von Werten für die Koordination und den Radius erzeugen zu können.
        /// Konstruktoren werden nicht vererbt: Obwohl in der Superklasse Figure ein parameterloser Konstruktor
        /// exisitert, muss dieser Konstruktor in der Subklasse definiert werden.
        /// </summary>
        public Circle() { }

        /// <summary>
        /// Mit dem NEW Modifizierer wird dem Compiler mitgeteilt, dass an dieser
        /// Stelle ABSICHTLICH der gleichnamige Member in der Basisklasse
        /// ausgeblendet wird.
        /// Es ist dadurch möglich, in einer Subklasse das Verhalten der Objekte
        /// spezifisch zu gestalten.
        /// Hier wird für ein Objekt der Klasse Circle der MITTELPUNKT ausgegeben
        /// (anstelle der linken, oberen Ecke bei allen anderen Figuren).
        /// </summary>
        public new void GetPosition()
        {
            Console.WriteLine(
                $"Center of circle is at x={_xPosition}, " +
                $"and y={_yPosition}"
            );
        }

        private new void PrintName()
        {
            Console.WriteLine("It's a CIRCLE");
        }

        public void PrintNameOfCircle()
        {
            this.PrintName();   // CIRCLE
            base.PrintName();   // FIGURE
        }

        public void PrintScaleFactorOfCircle()
        {
            Console.WriteLine($"Scale factor of circle is: {_scaleFactor}");

            // Jetzt wirds spannend :-)
            Console.WriteLine("---");
            Console.WriteLine($"Scale factor of circle is: {base._scaleFactor}");   // 1
            this.PrintScaleFactor();    // Methode geerbt von Figure-Klasse         // 1
            base.PrintScaleFactor();    // Methode in der Figure-Klasse             // 1
            Console.WriteLine("---");
        }


        #region POLYMORPHISM

        public override void Draw()
        {
            Console.WriteLine($"Drawing circle with radius={_radius}");
        }

        #endregion POLYMORPHISM

    }
}
